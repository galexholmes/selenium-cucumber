@regression

Feature: Esco Docreg Login feature

  Background:
    Given the esco docreg home page is loaded

  Scenario: verifies the user can login successfully
    When the user clicks the Login button
    And the user enters username "tdespenza" in the esco docreg Login page
    And the user enters password "Test1234!" in the esco docreg Login page
    And the user clicks the Sign In button in the esco docreg Login page
    Then the user should be successfully logged in

  Scenario: verifies the user cannot login successfully with an invalid username
    When the user clicks the Login button
    And the user enters username "tdespenza" in the esco docreg Login page
    And the user enters password "Test" in the esco docreg Login page
    And the user clicks the Sign In button in the esco docreg Login page
    Then the user should see error "Sorry, we were not able to find a user with that username and password." displayed

  Scenario: verifies the user cannot login successfully with an invalid password
    When the user clicks the Login button
    And the user enters username "gholmes" in the esco docreg Login page
    And the user enters password "Test1234!" in the esco docreg Login page
    And the user clicks the Sign In button in the esco docreg Login page
    Then the user should see error "Sorry, we were not able to find a user with that username and password." displayed




