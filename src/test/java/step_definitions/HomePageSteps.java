package step_definitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import objects.pages.HomePage;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class HomePageSteps extends AbstractStepDef {
  private final static Logger log = Logger.getLogger(HomePageSteps.class.getName());

	protected WebDriver driver;

	public HomePageSteps() {
		driver = Hooks.driver;
	}

    @Given("^the esco docreg home page is loaded$")
    public void theEscoDocregHomePageIsLoaded() {
        HomePage homePage = new HomePage(driver);
        homePage.getEscoURL();
    }
}