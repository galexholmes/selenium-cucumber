package step_definitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import objects.pages.HomePage;
import objects.pages.LoginPage;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class LoginPageSteps extends AbstractStepDef {
  private final static Logger log = Logger.getLogger(LoginPageSteps.class.getName());

	protected WebDriver driver;

	public LoginPageSteps() {
		driver = Hooks.driver;
	}


    @When("^the user clicks the Login button$")
    public void theUserClicksTheLoginButton() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickLoginButton();

    }

    @And("^the user enters username \"([^\"]*)\" in the esco docreg Login page$")
    public void theUserEntersUsernameInTheEscoDocregLoginPage(String text) throws Throwable {
      LoginPage loginPage = new LoginPage(driver);
      loginPage.enterUsername(text);
    }

    @And("^the user enters password \"([^\"]*)\" in the esco docreg Login page$")
    public void theUserEntersPasswordInTheEscoDocregLoginPage(String text) throws Throwable {
      LoginPage loginPage = new LoginPage(driver);
      loginPage.enterPassword(text);
    }

    @And("^the user clicks the Sign In button in the esco docreg Login page$")
    public void theUserClicksTheSignInButtonInTheEscoDocregLoginPage() throws InterruptedException {
      LoginPage loginPage = new LoginPage(driver);
      loginPage.clickSignIn();
    }

    @Then("^the user should be successfully logged in$")
    public void theUserShouldBeSuccessfullyLoggedIn() throws InterruptedException {
      LoginPage loginPage = new LoginPage(driver);
      loginPage.verifyLoginSuccessful();
    }


  @Then("^the user should see error \"([^\"]*)\" displayed$")
  public void theUserShouldSeeErrorDisplayed(String text) throws Throwable {
    LoginPage loginPage = new LoginPage(driver);
    loginPage.verifyErrormessage(text);
  }
}