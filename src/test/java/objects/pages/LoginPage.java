package objects.pages;

import helpers.Log;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static junit.framework.TestCase.assertTrue;


public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver,20);
    }


    public void enterUsername(String text) throws InterruptedException
    {
        Log.info("entering Username");
        enterText(By.id("username"),text);
    }

    public void enterPassword(String text) throws InterruptedException
    {
        Log.info("entering Password");
        enterText(By.id("password"),text);
    }
    public void clickSignIn() throws InterruptedException
    {
        Log.info("Clicking Sign In");
        clickElement(driver,By.xpath("//*[@class='btn btn-primary btn-raised']"),20);
    }
    public void clickLoginButton() throws InterruptedException
    {
        Log.info("Clicking Log In");
        clickElement(driver,By.xpath("//*[@href='/login/auth']"),20);
    }
    public void verifyLoginSuccessful() throws InterruptedException
    {
        Log.info("Verify Successful Login");
        assertTrue(elementIsDisplayed(By.xpath("//*[@href='/userSearch/index']")));
    }
    public void verifyErrormessage(String text) throws InterruptedException
    {
        Log.info("Verify Successful Login");
        assertTrue(elementIsDisplayed(By.xpath("//*[@class='alert alert-danger'][contains(text(),'"+text+"')]")));
    }


    }
























