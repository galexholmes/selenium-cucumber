package objects.pages;

import helpers.Log;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.*;
import java.util.ArrayList;

import static junit.framework.Assert.fail;
import static junit.framework.TestCase.assertTrue;


public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver,20);
    }

    public void getEscoURL(){
        String envName = "";
        if (!StringUtils.isEmpty(System.getProperty("esco.siteUrl"))) {
            envName = System.getProperty("esco.siteUrl");
        }
        final String loginUrl = "https://qa.escodocreg.com/home/index";
        driver.get(loginUrl);
    }
    }
























